module.exports = {
  purge: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  content: [
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        /* according to our custom graphical charter */
        'f-blue': {
          50: '#bdeff9',
          100: '#b0ecf7',
          200: '#a3e9f6',
          300: '#95e5f5',
          400: '#88e2f3',
          500: '#7bdff2',
          600: '#6fc9da',
          700: '#62b2c2',
          800: '#569ca9',
          900: '#4a8691',
        },
        'f-green': {
          50: '#f3fefc',
          100: '#dbfbf7',
          200: '#c2f9f2',
          300: '#aaf6ed',
          400: '#92f4e8',
          500: '#79f1e3',
          600: '#61efde',
          700: '#49ecd9',
          800: '#30ead4',
          900: '#18e7cf',
        },
        'magenta': {
          50: '#F6B9F4',
          100: '#F39DF0',
          200: '#EF82EC',
          300: '#EE74EA',
          400: '#EA58E5',
          500: '#E73DE2',
          600: '#DE1BD7',
          700: '#B917B3',
          800: '#941290',
          900: '#6F0E6C',
        }
      },
      minWidth: {
        '0': '0',
        '1/4': '25%',
        '1/3': '33%',
        '1/2': '50%',
        '3/4': '75%',
        'full': '100%',
       }
    },
  },
  variants: {
    extend: {
      ringColor: ['focus', 'hover'],
      border: ['focus', 'hover'],
      outline: ['focus']
    }
  },
  plugins: [
    require('flowbite/plugin')
  ]
}
