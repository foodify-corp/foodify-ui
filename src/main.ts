import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'mosha-vue-toastify/dist/style.css'
import './assets/styles/button.css'
import './assets/styles/card.css'
import './assets/styles/input.css'
import './index.css'
import 'flowbite'

createApp(App).use(store).use(router).mount('#app');
