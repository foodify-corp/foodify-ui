import { useStorage } from "@vueuse/core";

export const sidebarState: any = useStorage<boolean>('sidebarState', false);

export const toggleSidebar = (() => {
    sidebarState.value = !sidebarState.value
});
