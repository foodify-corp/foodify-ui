export const fixDecimal = (value: number, decimal: number): string => {
    return Number.parseFloat(value.toString()).toFixed(decimal);
}

export const convertNumberToPrice = (price: number): string => {
    return fixDecimal(price, 2) + ' €';
}

export const getGetterName = (moduleName: string, removeNumber: number): string => {
    return `${moduleName}/selected${moduleName[0].toUpperCase()}${moduleName.substring(1, moduleName.length-removeNumber)}`;
}