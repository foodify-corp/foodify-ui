export interface Restaurant {
    id: string
    name: string
    address: string
    city: string
    cp: string
    country: string
    activity: string
    n_siren: string
    kitchen_style: string
    take_away: boolean | null
    on_place: boolean | null
    instagram_url?: string
    facebook_url?: string
    twitter_url?: string
    created_at: string
    updated_at: string
}

export interface StateRestaurant {
    selectedRestaurant: Restaurant
    ownerRestaurants: Restaurant[]
    restaurants: Restaurant[]
    restaurant: Restaurant
}