export interface UserWorkAtRestaurant {
    id: string
    created_at: string
    updated_at: string
}