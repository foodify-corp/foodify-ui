import { Dish } from "@/@types";
import { Menu } from "@/@types";

export interface MenuDish {
    id: string
    price?: number
    dishes: Dish[]
    menu: Menu
    created_at: string
    updated_at: string
}