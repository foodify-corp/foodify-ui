export interface User {
    id: string
    email: string
    roles: string[]
    firstName: string | null
    lastName: string | null
    birthday: string | null
    phone: string | null
    picture: string | null
    is_shown: boolean | null
    facebook_url: string | null
    instagram_url: string | null
    twitter_url: string | null
    created_at: string
    updated_at: string
}

export interface Auth {
    token: string
    refresh_token: string
}

export interface StateUser {
    users: User[]
    user: User
    auth: Auth
}