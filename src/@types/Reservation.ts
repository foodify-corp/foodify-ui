export interface Reservation {
    id: string
    reservation_date: string
    nb_people: number | null
    is_validated: boolean | null
    confirmed_at?: string
    status: string
    table_id?: string
    name: string
    created_at: string
    updated_at: string
}

export interface StateReservation {
    reservations: Reservation[],
    selectedReservation: Reservation
}