export interface Menu {
    id: string
    name: string
    display: boolean
    created_at: string
    updated_at: string
}

export interface StateMenu {
    menus: Menu[]
    menu: Menu
}
