import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Restaurants from '../views/Restaurants.vue'
import RestaurantSingle from '../views/Restaurant.vue'
import DashboardAdmin from '../views/Admin/DashboardAdmin.vue'
import Restaurant from '../views/Admin/Restaurant.vue'
import Reservation from '../views/Admin/Reservation.vue'
import DishMenuCategory from '../views/Admin/DishMenuCategory.vue'
import Table from '../views/Admin/Table.vue'
import UsersRestaurant from '../views/Admin/UsersRestaurant.vue'
import store from '../store/index';
import { computed } from "vue";
import { useStorage } from "@vueuse/core";

const owner = computed(() => store.getters['users/owner']);
export const lastVisitedRoute: any = useStorage('lastVisitedRoute', '');

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/restaurants',
        name: 'RestaurantsList',
        component: Restaurants
    },
    {
        path: '/restaurants/:id',
        name: 'Restaurant',
        component: RestaurantSingle
    },
    {
        path: '/owner',
        name: 'Owner',
        component: DashboardAdmin,
        meta: { isOwner: true },
        children: [
            {
                path: 'restaurant',
                name: 'ManageRestaurant',
                component: Restaurant
            },
            {
                path: 'reservation',
                name: 'ManageReservation',
                component: Reservation
            },
            {
                path: 'dishMenuCategory',
                name: 'ManageDishMenuCategory',
                component: DishMenuCategory
            },
            {
                path: 'table',
                name: 'ManageTable',
                component: Table
            },
            {
                path: 'usersRestaurant',
                name: 'ManageUsersRestaurant',
                component: UsersRestaurant
            },
        ]
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach((to, from) => {
    // Check if user is connected
    // Route backend bearer token userId
    if(to?.meta?.isOwner && !owner.value) {
        return {
            path: '/',
            // save the location we were at to come back later
            query: { redirect: to.fullPath },
        }
    }
});

router.afterEach(to => {
    if (to.name !== 'Home') {
        lastVisitedRoute.value = to.name;
    }
});

export default router
