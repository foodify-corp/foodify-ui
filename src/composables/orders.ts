// import store from '@/store';
// import { Dish } from '@/@types';
// import { OrderDishes } from '@/@types';
// import { Order } from '@/@types';
//
// export const getCurrentOrder = (): Order => {
//     const currentOrder = store.getters['users/currentOrder'];
//     return currentOrder;
// }
//
// export const addToCurrentOrder = (dish: Dish): Order => {
//     const currentOrder = getCurrentOrder();
//     const orderDishes = currentOrder['order_dishes'];
//     const dishToAdd = {
//         quantity: 1,
//         dish: dish,
//     }
//     const idDishesAlreadyOrdered = orderDishes.map(x => x.dish.id);
//
//     if (!idDishesAlreadyOrdered.includes(dish.id)) {
//         orderDishes.push(dishToAdd);
//     } else {
//         orderDishes.forEach((element: OrderDishes) => {
//             if (element.dish.id === dish.id) {
//                 element.quantity = element.quantity + 1;
//             }
//         } )
//     }
//
//     return currentOrder;
// }
//
// export const deleteFromCurrentOrder = (index: number): OrderDishes[] => {
//     const orderedDishes = getCurrentOrder()['order_dishes'];
//     orderedDishes.splice(index, 1);
//     return orderedDishes;
// }
//
