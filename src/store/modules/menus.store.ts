import { Menu, StateMenu } from "@/@types";
import { ActionContext, ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService"

const state: StateMenu = {
    menus: [],
    menu: {
        id: '',
        name: '',
        display: false,
        created_at: '',
        updated_at: ''
    }
}

const getters: GetterTree<StateMenu, RootState> = {
    selectedMenu: state => state.menu,
    menus: state => state.menus
}

const mutations: MutationTree<StateMenu> = {
    GET_MENUS(state: StateMenu, menus: Menu[]) {
        state.menus = menus;
    },
    ADD_MENU(state: StateMenu, menu: Menu) {
        state.menus.push(menu);
    },
    UPDATE_MENU(state: StateMenu, updatedMenu: Menu) {
        const menuToUpdate = state.menus.findIndex(menu => menu.id === updatedMenu.id);
        state.menus.splice(menuToUpdate, 1, updatedMenu);
    },
    DELETE_MENU(state: StateMenu, deletedMenuId: string) {
        const menuToDelete = state.menus.findIndex(menu => menu.id === deletedMenuId);
        state.menus.splice(menuToDelete, 1);
    },
    SET_CURRENT_MENU(state: StateMenu, menu: Menu) {
        state.menu = menu;
    }
}

const actions: ActionTree<StateMenu, RootState> = {
    // TODO Passer le restaurantId en payload
    async getMenus({commit}: ActionContext<StateMenu, RootState>, restaurantId: string) {
        const res = await api.getMenus(restaurantId);
        commit('GET_MENUS', res.menus);
    },
    async addMenu({commit}: ActionContext<StateMenu, RootState>, payload) {
        const res = await api.addMenu(payload.restaurantId, payload.menu);
        commit('ADD_MENU', res.menu);
    },
    async updateMenu({commit}: ActionContext<StateMenu, RootState>, payload) {
        const res = await api.updateMenu(payload.restaurantId, payload.menu);
        commit('UPDATE_MENU', res.menu);
    },
    async deleteMenu({commit}: ActionContext<StateMenu, RootState>, payload) {
        const res = await api.deleteMenu(payload.restaurantId, payload.menuId)
        commit('DELETE_MENU', res.deleted_menu_id);
    },
    setCurrentMenu({commit}: ActionContext<StateMenu, RootState>, menu: Menu) {
        commit('SET_CURRENT_MENU', menu);
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
