import { ActionContext, ActionTree, GetterTree, MutationTree } from "vuex";
import { RootState } from "@/store";
import { Category, StateCategory } from "@/@types";
import api from "@/services/api/ApiService";

const state: StateCategory = {
    category: {
        id: '',
        name: '',
        created_at: '',
        updated_at: ''
    },
    categories: []
}

const getters: GetterTree<StateCategory, RootState> = {
    categories: (state: StateCategory) => state.categories,
}

const mutations: MutationTree<StateCategory> = {
    CATEGORIES(state, categories: Category[]) {
        state.categories = categories;
    },
    ADD_CATEGORY(state, category: Category) {
        state.categories.push(category);
    },
    UPDATE_CATEGORY(state, updatedCategory: Category) {
        const categoryToUpdateIndex = state.categories.findIndex(category=> category.id === updatedCategory.id);
        state.categories.splice(categoryToUpdateIndex, 1, updatedCategory);
    },
    DELETE_CATEGORY(state, deletedCategoryId: string) {
        const categoryToDeleteIndex = state.categories.findIndex(category=> category.id === deletedCategoryId);
        state.categories.splice(categoryToDeleteIndex, 1);
    }
}

const actions: ActionTree<StateCategory, RootState> = {
    async getCategories({commit}: ActionContext<StateCategory, RootState>, restaurantId: string) {
        try {
            const res = await api.getCategories(restaurantId);
            commit('CATEGORIES', res.categories);
        } catch (e) {
            throw e.message;
        }
    },
    async addCategory({commit}: ActionContext<StateCategory, RootState>, payload: any) {
        try {
            const res = await api.addCategory(payload.restaurantId, payload.category);
            commit('ADD_CATEGORY', res.category);
        } catch (e) {
            throw e.message;
        }
    },
    async updateCategory({commit}: ActionContext<StateCategory, RootState>, payload: any) {
        try {
            const res = await api.updateCategory(payload.restaurantId, payload.dish);
            commit('UPDATE_CATEGORY', res.category);
        } catch (e) {
            throw e.message;
        }
    },
    async deleteCategory({commit}: ActionContext<StateCategory, RootState>, payload: any) {
        try {
            const res = await api.deleteCategory(payload.restaurantId, payload.categoryId);
            commit('DELETE_CATEGORY', res.deleted_category_id);
        } catch (e) {
            throw e.message;
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}
