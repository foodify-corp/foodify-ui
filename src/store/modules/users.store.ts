import { StateUser, User, Auth } from "@/@types";
import { ActionTree, ActionContext, MutationTree, GetterTree } from "vuex";
import { RootState } from "@/store";
import api from "@/services/api/ApiService";
import { user } from "@/services/api";

const state: StateUser = {
    user: {
        id: '',
        email: '',
        roles: [],
        firstName: '',
        lastName: '',
        birthday: '',
        phone: '',
        picture: '',
        is_shown: null,
        facebook_url: '',
        instagram_url: '',
        twitter_url: '',
        created_at: '',
        updated_at: ''
    },
    users: [],
    auth: {
        token: '',
        refresh_token: ''
    }
};

const getters: GetterTree<StateUser, RootState> = {
    users: (state: StateUser) => state.users,
    connectedUser: (state: StateUser) => state.user,
    auth: (state: StateUser) => state.auth,
    isConnected: (state: StateUser) => state.user.roles.length > 0,
    owner: (state: StateUser) => state.user.roles.includes("ROLE_OWNER"),
    isUser: (state: StateUser) => state.user.roles.length === 1 && state.user.roles.includes("ROLE_USER")
}

const mutations: MutationTree<StateUser> = {
    LOGIN: (state: StateUser, auth: Auth) => {
        state.auth = auth;
    },
    LOGOUT: (state: StateUser) => {
        state.auth = {
            token: '',
            refresh_token: ''
        };

        state.user = {
            id: '',
            email: '',
            roles: [],
            firstName: '',
            lastName: '',
            birthday: '',
            phone: '',
            picture: '',
            is_shown: null,
            facebook_url: '',
            instagram_url: '',
            twitter_url: '',
            created_at: '',
            updated_at: ''
        }
    },
    SET_USER_DATA: (state: StateUser, user: User) => {
        state.user = user;
    },
    SET_USERS_DATA: (state: StateUser, users: User[]) => {
        state.users = users;
    }
}

const actions: ActionTree<StateUser, RootState> = {
    async login({ commit, dispatch }: ActionContext<StateUser, RootState>, payload) {
        try {
            const auth = await api.login(payload);
            commit('LOGIN', auth);
        } catch (e) {
            throw e.data;
        }
    },
    async logout({ commit }: ActionContext<StateUser, RootState>) {
        // eslint-disable-next-line no-useless-catch
        try {
            const auth = await api.logout();
            user.value = {};
            commit('LOGOUT', auth);
        } catch (e) {
            throw e;
        }
    },
    async refreshToken({ commit }: ActionContext<StateUser, RootState>, refreshToken: string) {
        // eslint-disable-next-line no-useless-catch
        try {
            const auth = await api.refreshToken({ refresh_token: refreshToken });
            commit('LOGIN', auth);
        } catch (e) {
            throw e;
        }
    },
    async userInformation({ commit }: ActionContext<StateUser, RootState>) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getUserInformation();
            commit('SET_USER_DATA', res.user)
        } catch (e) {
            throw e;
        }
    },
    async updateUsers({ commit }: ActionContext<StateUser, RootState>) {
        // eslint-disable-next-line no-useless-catch
        try {
            const res = await api.getAllUsers();
            if (res !== null) {
                commit('SET_USERS_DATA', res.users);
            }
        } catch (e) {
            throw e;
        }
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
}
