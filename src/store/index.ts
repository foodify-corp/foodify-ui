import { createStore } from 'vuex';
import { StateUser, StateDish, StateOrder, StateRestaurant, StateReservation, StateCategory, StateTable, StateMenu } from '@/@types';
import users from './modules/users.store';
import dishes from './modules/dishes.store';
import orders from './modules/orders.store';
import restaurants from './modules/restaurants.store'
import reservations from './modules/reservations.store'
import categories from './modules/category.store'
import tables from './modules/tables.store'
import menus from './modules/menus.store'

export default createStore({
  modules: {
    users,
    dishes,
    orders,
    restaurants,
    reservations,
    categories,
    tables,
    menus
  }
})

export interface RootState {
  users: StateUser
  restaurants: StateRestaurant
  reservations: StateReservation
  dishes: StateDish
  orders: StateOrder
  categories: StateCategory,
  tables: StateTable,
  menus: StateMenu
}
