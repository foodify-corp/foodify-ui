import { restaurantOriginalState } from "@/originalState/restaurant";
import { tableOriginalState } from "@/originalState/table";
import { dishOriginalState } from "@/originalState/dish";
import { reservationOriginalState } from "@/originalState/reservation";

export {
    restaurantOriginalState,
    tableOriginalState,
    dishOriginalState,
    reservationOriginalState
}
