export const reservationOriginalState = {
    id: '',
    reservation_date: '',
    nb_people: null,
    is_validated: null,
    confirmed_at: '',
    status: '',
    table_id: '',
    name: '',
    created_at: '',
    updated_at: ''
}
