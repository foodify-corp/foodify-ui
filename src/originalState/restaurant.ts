export const restaurantOriginalState = {
    id: '',
    name: '',
    address: '',
    city: '',
    cp: '',
    country: '',
    activity: '',
    n_siren: '',
    kitchen_style: '',
    take_away: false,
    on_place: false,
    instagram_url: '',
    twitter_url: '',
    facebook_url: ''
}
