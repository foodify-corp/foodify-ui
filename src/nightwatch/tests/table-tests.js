// eslint-disable-next-line no-undef
describe('Table CRUD', function() {
    // eslint-disable-next-line no-undef
    it('Create Table', async function(browser) {
        const table = browser.page.ownerTable();
        const tableNumber = Math.floor(Math.random() * 10000);
        const tablePlaces = Math.floor(Math.random() * 10);

       await table
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        table
            .waitForElementVisible('@ownerRestaurants')
            .click('@ownerRestaurants')
            .selectRestaurant()

        table.waitForElementVisible('@ownerTables')
            .click('@ownerTables')

        const result = await table.findElements('.table-row-item');

        table.createTable(tableNumber, tablePlaces, 1);

        // Assertions - Expect
        table.expect.elements('.table-row-item').count.to.equal(result.length +1)

        browser.end()
    });

    // eslint-disable-next-line no-undef
    it('Update Table', async function(browser) {
        const table = browser.page.ownerTable();
        const tableNumber = Math.floor(Math.random() * 10000);
        const tablePlaces = Math.floor(Math.random() * 10);

        await table
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        table
            .waitForElementVisible('@ownerRestaurants')
            .click('@ownerRestaurants')
            .selectRestaurant()

        table.waitForElementVisible('@ownerTables')
            .click('@ownerTables')

        table.updateTable(tableNumber, tablePlaces);

        // Assertions - Expect
        table.assert.textContains('table > tbody > tr:first-child', tableNumber.toString())

        browser.end()
    });

    // eslint-disable-next-line no-undef
    it('Delete Table', async function(browser) {
        const table = browser.page.ownerTable();

        await table
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        table
            .waitForElementVisible('@ownerRestaurants')
            .click('@ownerRestaurants')
            .selectRestaurant()

        table.waitForElementVisible('@ownerTables')
            .click('@ownerTables')

        const result = await table.findElements('.table-row-item');

        table.deleteTable();

        // Assertions - Expect
        table.expect.elements('.table-row-item').count.to.equal(result.length -1)

        browser.end()
    });
});


