// eslint-disable-next-line no-undef
describe('Restaurant CRUD', function() {
    // eslint-disable-next-line no-undef
    it('Create restaurant', function(browser) {
        const restaurant = browser.page.ownerRestaurant();
        const address = `${new Date().getTime()}`;

        restaurant
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        restaurant
            .waitForElementVisible('@ownerRestaurants')
            .click('@ownerRestaurants')
            .createRestaurant(address)

        restaurant.assert.textContains('table', address)

        browser.end()
    });

    // eslint-disable-next-line no-undef
    it('Update restaurant', function(browser) {
        const restaurant = browser.page.ownerRestaurant();
        const address = `${new Date().getTime()}`;

        restaurant
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        restaurant
            .waitForElementVisible('@ownerRestaurants')
            .click('@ownerRestaurants')
            .updateRestaurant(address)

        restaurant.assert.textContains('table', address);

        browser.end();
    })

    // eslint-disable-next-line no-undef
    it('Delete restaurant', async function(browser) {
        const restaurant = browser.page.ownerRestaurant();

        await restaurant
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        restaurant
            .waitForElementVisible('@ownerRestaurants')
            .click('@ownerRestaurants')

        const result = await restaurant.findElements('css selector', '.table-row-item');

        restaurant.deleteRestaurant();

        // Assertions - Expect
        restaurant.expect.elements('.table-row-item').count.to.equal(result.length - 1);

        browser.end()
    })
});


