// eslint-disable-next-line no-undef
describe('Reservation CRUD', function() {
    // eslint-disable-next-line no-undef
    it('Accept, Refuse, Cancel & available reservation', function(browser) {
        const reservation = browser.page.ownerReservation();

        reservation
            .navigate()
            .login(browser.globals.ownerCredential.email, browser.globals.ownerCredential.password);

        reservation
            .selectRestaurant()

        // Right custom command to select reservation
        reservation
            .waitForElementVisible('@ownerReservation')
            .click('@ownerReservation')
            .click('table > tbody > tr:first-child')

        reservation.acceptReservation();
        reservation.assert.textContains('table > tbody > tr:first-child', 'Accepted')
        // assert first resa is accepted
        reservation.refuseReservation();
        reservation.assert.textContains('table > tbody > tr:first-child', 'Refused')
        // assert first resa is refused
        reservation.cancelReservation();
        reservation.assert.textContains('table > tbody > tr:first-child', 'Canceled')
        // assert first resa is canceled
        reservation.availableReservation();
        reservation.assert.textContains('table > tbody > tr:first-child', 'Available')
        // assert first resa is available

        browser.end()
    });
});