const restaurantCommands = {
    createRestaurant: function(address) {
        return this
            .click('@createRestaurant')
            .setValue('@restaurantInputName', 'MoxxyTheCat')
            .setValue('@restaurantInputAddress', address)
            .setValue('@restaurantInputCity', 'Texas')
            .setValue('@restaurantInputCp', '98751')
            .setValue('@restaurantInputCountry', 'USA')
            .setValue('@restaurantInputActivity', 'Food')
            .setValue('@restaurantInputKitchenStyle', 'French')
            .setValue('@restaurantInputSiren', '9874DEFR8eFe7')
            .setValue('@restaurantInputInstagram', 'instagram/moxxy')
            .setValue('@restaurantInputTwitter', 'twitter/moxxy')
            .setValue('@restaurantInputFacebook', 'facebook/moxxy')
            .click('#save')
    },
    updateRestaurant: function(address) {
        return this
            .click('table > tbody > tr:last-child')
            .waitForElementVisible('@updateRestaurant')
            .click('@updateRestaurant')
            .setValue('@restaurantInputAddress', address)
            .click('#save')
    },
    deleteRestaurant: function() {
        return this
            .click('table > tbody > tr:last-child')
            .waitForElementVisible('@deleteRestaurant')
            .getLocationInView('css selector', '@deleteRestaurant')
            .click('@deleteRestaurant')
    }
}

module.exports = {
    commands: [restaurantCommands],
    url: function() {
        return browser.globals.serverUrl;
    },
    elements: {
        ownerRestaurants: {
            selector: '#owner-restaurants'
        },
        createRestaurant: {
            selector: '#create-restaurant'
        },
        updateRestaurant: {
            selector: '#update-restaurant'
        },
        deleteRestaurant: {
            selector: '#delete-restaurant'
        },
        restaurantInputName: {
            selector: '#restaurant-input-name'
        },
        restaurantInputAddress: {
            selector: '#restaurant-input-address'
        },
        restaurantInputCity: {
            selector: '#restaurant-input-city'
        },
        restaurantInputCp: {
            selector: '#restaurant-input-cp'
        },
        restaurantInputCountry: {
            selector: '#restaurant-input-country'
        },
        restaurantInputActivity: {
            selector: '#restaurant-input-activity'
        },
        restaurantInputKitchenStyle: {
            selector: '#restaurant-input-kitchen-style'
        },
        restaurantInputSiren: {
            selector: '#restaurant-input-siren'
        },
        restaurantInputInstagram: {
            selector: '#restaurant-input-instagram'
        },
        restaurantInputTwitter: {
            selector: '#restaurant-input-twitter'
        },
        restaurantInputFacebook: {
            selector: '#restaurant-input-facebook'
        }
    }
}