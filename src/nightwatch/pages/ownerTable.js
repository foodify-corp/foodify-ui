const dishCommands = {
    createTable: function(tableNumber, tablePlaces, tableQuantity) {
        return this
            .click('@createTable')
            .setValue('@tableInputTableNumber', tableNumber)
            .setValue('@tableInputTablePlaces', tablePlaces)
            .setValue('@tableInputTableQuantity', tableQuantity)
            .click('#save')
    },
    updateTable: function(tableNumber, tablePlaces) {
        return this
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@updateTable')
            .click('@updateTable')
            .setValue('@tableInputTableNumber', tableNumber)
            .setValue('@tableInputTablePlaces', tablePlaces)
            .click('#save')
    },
    deleteTable: function() {
        return this
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@deleteTable')
            .getLocationInView('css selector', '@deleteTable')
            .click('@deleteTable')
    }
}

module.exports = {
    commands: [dishCommands],
    url: function() {
        return browser.globals.serverUrl;
    },
    elements: {
        ownerRestaurants: {
            selector: '#owner-restaurants'
        },
        ownerTables: {
            selector: '#owner-restaurant-tables'
        },
        createTable: {
            selector: '#create-table'
        },
        updateTable: {
            selector: '#update-table'
        },
        deleteTable: {
            selector: '#delete-table'
        },
        tableInputTableNumber: {
            selector: '#table-input-table-number'
        },
        tableInputTablePlaces: {
            selector: '#table-input-table-places'
        },
        tableInputTableQuantity: {
            selector: '#table-input-table-quantity'
        }
    }
}