const dishCommands = {
    createDish: function(price) {
        return this
            .waitForElementVisible('@ownerDishes')
            .click('@ownerDishes')
            .click('@createDish')
            .setValue('@dishInputName', 'MoxxyTheCat')
            .setValue('@dishInputPrice', price)
            .click('@dishInputAvailable')
            .waitForElementVisible('#save')
            .click('#save')
    },
    updateDish: function(price) {
        return this
            .waitForElementVisible('@ownerDishes')
            .click('@ownerDishes')
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@updateDish')
            .click('@updateDish')
            .setValue('@dishInputPrice', price)
            .waitForElementVisible('#save')
            .click('#save')
    },
    deleteDish: function() {
        return this
            .waitForElementVisible('@ownerDishes')
            .click('@ownerDishes')
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@deleteDish')
            .getLocationInView('css selector', '@deleteDish')
            .click('@deleteDish')
    }
}

module.exports = {
    commands: [dishCommands],
    url: function() {
        return browser.globals.serverUrl;
    },
    elements: {
        ownerRestaurants: {
            selector: '#owner-restaurants'
        },
        ownerDishes: {
            selector: '#owner-restaurant-dishes'
        },
        tableRowItem: {
            selector: ".table-row-item"
        },
        createDish: {
            selector: '#create-dish'
        },
        updateDish: {
            selector: '#update-dish'
        },
        deleteDish: {
            selector: '#delete-dish'
        },
        dishInputName: {
            selector: '#dish-input-name'
        },
        dishInputPrice: {
            selector: '#dish-input-price'
        },
        dishInputAvailable: {
            selector: '#dish-input-available'
        }
    }
}