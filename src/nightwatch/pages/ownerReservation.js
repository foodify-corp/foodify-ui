const reservationCommands = {
    acceptReservation: function() {
        return this
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@acceptReservation')
            .click('@acceptReservation')
    },
    refuseReservation: function() {
        return this
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@refuseReservation')
            .click('@refuseReservation')
    },
    cancelReservation: function() {
        return this
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@cancelReservation')
            .click('@cancelReservation')
    },
    availableReservation: function() {
        return this
            .click('table > tbody > tr:first-child')
            .waitForElementVisible('@availableReservation')
            .click('@availableReservation')
    }
}

module.exports = {
    commands: [reservationCommands],
    url: function() {
        return browser.globals.serverUrl;
    },
    elements: {
        ownerReservation: {
            selector: '#owner-restaurant-reservations'
        },
        acceptReservation: {
            selector: '#update-reservation-accepted'
        },
        refuseReservation: {
            selector: '#update-reservation-refused'
        },
        cancelReservation: {
            selector: '#update-reservation-canceled'
        },
        availableReservation: {
            selector: '#update-reservation-available'
        }
    }
}