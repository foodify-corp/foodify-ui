module.exports = {
    command: function (email, password) {
        return this.click("#app > div.navbar.flex.fixed.flex-row.bg-white.border-b.border-gray-200.w-screen.z-20 > div > div > div:nth-child(1) > span")
            .waitForElementVisible('.popper')
            .click('#login')
            .setValue('#input-email', email)
            .setValue('#input-password', password)
            .click('#connection')
    }
}