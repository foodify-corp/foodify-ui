module.exports = {
    command: function () {
        return this.click("#owner-restaurants")
            .waitForElementVisible('table')
            .useXpath().click("//td[text()='Le Pollet infernal']")
            .useCss()
    }
}