import { User, Auth, Dish } from '@/@types'


export const getAllDishes = (): Dish[] =>  [
    {
        id: 'aaa-111',
        name: 'Certified Veggie Bacon Lover',
        price: 11.90,
        is_available: true,
        is_available_time: ['midday', 'evening'],
        created_at: "1642148044",
        updated_at: "1642148044",
        dishes_categories: ['Plats chauds', 'Plats froids'],
        restaurants_id: '000',
    },
    {
        id: "bbb-222",
        name: 'Voilà de la boulette (falafels) !',
        price: 11.90,
        is_available: true,
        is_available_time: ['midday', 'evening'],
        created_at: "1642148044",
        updated_at: "1642148044",
        dishes_categories: ['Plats chauds'],
        restaurants_id: '000',
    },
    {
        id: 'ccc-333',
        name: 'Sau mon name, sau mon name',
        price: 11.90,
        is_available: true,
        is_available_time: ['midday', 'evening'],
        created_at: "1642148044",
        updated_at: "1642148044",
        dishes_categories: ['Plats froids'],
        restaurants_id: '000',
    },
];
