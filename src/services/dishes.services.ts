// import store from '@/store';
import { Dish, Category } from '@/@types';


export const getAllDishes = (): Dish[] => {
    // const allDishes = store.getters['dishes/allDishes'];
    const allDishes = [{
        id: 'aaa-111',
        name: 'Certified Veggie Bacon Lover',
        price: 11.90,
        is_available: true,
        is_available_time: ['midday', 'evening'],
        created_at: "1642148044",
        updated_at: "1642148044",
        dishes_categories: ['Plats chauds', 'Plats froids'],
        restaurants_id: '000',
    },
    {
        id: 'bbb-222',
        name: 'Voilà de la boulette (falafels) !',
        price: 11.90,
        is_available: true,
        is_available_time: ['midday', 'evening'],
        created_at: "1642148044",
        updated_at: "1642148044",
        dishes_categories: ['Plats chauds'],
        restaurants_id: '000',
    },
    {
        id: 'ccc-333',
        name: 'Sau mon name, sau mon name',
        price: 11.90,
        is_available: true,
        is_available_time: ['midday', 'evening'],
        created_at: "1642148044",
        updated_at: "1642148044",
        dishes_categories: ['Plats froids'],
        restaurants_id: '000',
    }];
    return allDishes;
}

export const getDishesByCategory = (dishe_type_name: string): Dish[] => {
    const allDishes = getAllDishes();
    const filteredDishesByCategory: Dish[] = [];
    allDishes.forEach((element: Dish) => {
        if (element.dishes_categories.includes(dishe_type_name)) {
            filteredDishesByCategory.push(element);
        }
    })
    return filteredDishesByCategory;
}

export const getAllCategories = (): Category[] => {
    const dishesCategories = [
        {
            id: 'xxx-111',
            name: 'Plats chauds',
            created_at: '',
            updated_at: '',
        },
        {
            id: 'xxx-222',
            name: 'Plats froids',
            created_at: '',
            updated_at: '',
        },
        {
            id: 'xxx-333',
            name: 'Desserts',
            created_at: '',
            updated_at: '',
        },
        {
            id: 'xxx-444',
            name: 'Boissons',
            created_at: '',
            updated_at: '',
        },
    ]
    return dishesCategories;
}