import { ApiBaseService } from "@/services/api/ApiService";
import { User } from "@/@types";

export const ApiUserService = (superclass: typeof ApiBaseService) => class extends superclass {

    getAllUsers = async (): Promise<User[]> => {
        const uri = '/users';
        try {
            const response = await this.client().get(uri);
            return response.data.users;
        } catch (e) {
            throw e.response;
        }
    }

    getUserInformation = async (): Promise<User>  => {
        const uri = '/users/me';
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }
}
