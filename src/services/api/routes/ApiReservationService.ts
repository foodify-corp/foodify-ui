import { ApiBaseService } from "@/services/api/ApiService";
import { Reservation } from "@/@types";

export const ApiReservationService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getReservations(restaurantId: string): Promise<Reservation> {
        const uri = `/restaurant/${restaurantId}/reservation`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async addReservation(restaurantId: string, reservation: Partial<Reservation>): Promise<Reservation> {
        const uri = `/restaurant/${restaurantId}/reservation`;
        try {
            const res = await this.client().post(uri, reservation);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async updateReservation(restaurantId: string, reservation: Reservation): Promise<Reservation> {
        const uri = `/restaurant/${restaurantId}/reservation/${reservation.id}`;
        try {
            const res = await this.client().put(uri, reservation);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }
}
