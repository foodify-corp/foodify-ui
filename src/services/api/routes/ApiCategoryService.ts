import { ApiBaseService } from "@/services/api/ApiService";
import { Category } from "@/@types";

export const ApiCategoryService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getCategories(restaurantId: string) {
        const uri = `/restaurant/${restaurantId}/category`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async addCategory(restaurantId: string, category: Partial<Category>) {
        const uri = `/restaurant/${restaurantId}/category`;
        try {
            const res = await this.client().post(uri, category);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async updateCategory(restaurantId: string, category: Category) {
        const uri = `/restaurant/${restaurantId}/category/${category.id}`;
        try {
            const res = await this.client().post(uri, category);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async deleteCategory(restaurantId: string, categoryId: string) {
        const uri = `/restaurant/${restaurantId}/category/${categoryId}`;
        try {
            const res = await this.client().delete(uri);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }
}
