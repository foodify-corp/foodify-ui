import { ApiBaseService } from "@/services/api/ApiService";
import { Restaurant } from "@/@types";

export const ApiRestaurantService = (superclass: typeof ApiBaseService) => class extends superclass {

    addRestaurant = async (restaurant: Restaurant): Promise<Restaurant> => {
        const uri = '/restaurant/';
        try {
            const response = await this.client().post(uri, restaurant);
            return response.data;
        } catch (error) {
            throw error.response;
        }
    };

    getRestaurants = async (): Promise<Restaurant[]> => {
        const uri = `/restaurant`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    };

    getRestaurant = async (restaurantId: string): Promise<Restaurant> => {
        const uri = `/restaurant/${restaurantId}`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    };

    getOwnerRestaurant = async (restaurant: Restaurant): Promise<Restaurant> => {
        const uri = `/users/restaurant/${restaurant.id}`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    };

    getOwnerRestaurants = async (): Promise<Restaurant[]> => {
        const uri = `/users/restaurant`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    };

    updateRestaurant = async (restaurant: Restaurant): Promise<Restaurant> => {
        const uri = `/restaurant/${restaurant.id}`;
        try {
            const response = await this.client().put(uri, restaurant);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    };

    deleteRestaurant = async (restaurant: Restaurant): Promise<{ deleted_restaurant_id: string, message: string }> => {
        const uri = `/restaurant/${restaurant.id}`;
        try {
            const response = await this.client().delete(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    };
}
