import { ApiBaseService } from "@/services/api/ApiService";
import { Dish } from "@/@types";

export const ApiDishService = (superclass: typeof ApiBaseService) => class extends superclass {

    async getDishes(restaurantId: string) {
        const uri = `/restaurant/${restaurantId}/dish`;
        try {
            const res = await this.client().get(uri);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async addDish(restaurantId: string, dish: Dish) {
        const uri = `/restaurant/${restaurantId}/dish`;
        try {
            const res = await this.client().post(uri, dish);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async updateDish(restaurantId: string, dish: Dish) {
        const uri = `/restaurant/${restaurantId}/dish/${dish.id}`;
        try {
            const res = await this.client().put(uri, dish);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }

    async deleteDish(restaurantId: string, dishId :string) {
        const uri = `/restaurant/${restaurantId}/dish/${dishId}`;
        try {
            const res = await this.client().delete(uri);
            return res.data;
        } catch (e) {
            throw e.response;
        }
    }
}
