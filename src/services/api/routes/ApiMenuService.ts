import { ApiBaseService } from "@/services/api/ApiService";
import { Menu } from "@/@types";

export const ApiMenuService = (superclass: typeof ApiBaseService) => class extends superclass {

    getMenus = async (restaurantId: string) => {
        const uri = `/restaurant/${restaurantId}/menu`;
        try {
            const response = await this.client().get(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }

    addMenu = async (restaurantId: string, menu: Menu) => {
        const uri = `/restaurant/${restaurantId}/menu`;
        try {
            const response = await this.client().post(uri, menu);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }

    updateMenu = async (restaurantId: string, menu: Menu) => {
        const uri = `/restaurant/${restaurantId}/menu/${menu.id}`;
        try {
            const response = await this.client().put(uri, menu);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }

    deleteMenu = async (restaurantId: string, menuId: string) => {
        const uri = `/restaurant/${restaurantId}/menu/${menuId}`;
        try {
            const response = await this.client().delete(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }
}
