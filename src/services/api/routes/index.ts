import { ApiUserService } from "@/services/api/routes/ApiUserService";
import { ApiLoginService } from "@/services/api/routes/ApiLoginService";
import { ApiRestaurantService } from "@/services/api/routes/ApiRestaurantService";
import { ApiTableService } from "@/services/api/routes/ApiTableService";
import { ApiReservationService } from "@/services/api/routes/ApiReservationService";
import { ApiDishService } from "@/services/api/routes/ApiDishService";
import { ApiCategoryService } from "@/services/api/routes/ApiCategoryService";
import { ApiMenuService } from "@/services/api/routes/ApiMenuService";

export {
    ApiUserService,
    ApiLoginService,
    ApiRestaurantService,
    ApiTableService,
    ApiReservationService,
    ApiDishService,
    ApiCategoryService,
    ApiMenuService
}
