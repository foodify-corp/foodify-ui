import { ApiBaseService } from "@/services/api/ApiService";
import { Auth } from "@/@types";

export const ApiLoginService = (superclass: typeof ApiBaseService) => class extends superclass {
    login = async (payload: any): Promise<Auth> => {
        const uri = '/login';
        try {
            const response = await this.clientWithoutAuth.post(uri, payload);
            await this.setUserTokens(response.data);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }

    logout = async (): Promise<void> => {
        const uri = '/logout';
        try {
            const response = await this.client().post(uri);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }

    refreshToken = async (refreshToken: string): Promise<Auth> => {
        const uri = '/token/refresh';
        try {
            const response = await this.client().post(uri, refreshToken);
            await this.setUserTokens(response.data);
            return response.data;
        } catch (e) {
            throw e.response;
        }
    }
}
