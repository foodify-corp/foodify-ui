import { flowRight } from "lodash";
import {
    ApiLoginService,
    ApiUserService,
    ApiRestaurantService,
    ApiReservationService,
    ApiDishService,
    ApiTableService,
    ApiCategoryService,
    ApiMenuService
} from "@/services/api/routes";

import axios, { AxiosInstance } from "axios";
import client, { user } from "./index";

export class ApiBaseService {

    private configWithoutAuth = {
        baseURL: process.env.VUE_APP_API_BASE_URL,
        headers: {
            'Content-Type': 'application/json'
        }
    }

    setUserTokens = async (tokens: Record<string, string>): Promise<void> => {
        user.value.token = `Bearer ${tokens.token}`;
        user.value.refresh_token = tokens.refresh_token;
    }

    client() {
        return client
    }

    clientWithoutAuth: AxiosInstance = axios.create(this.configWithoutAuth);
}

export const ApiService = flowRight([ApiLoginService, ApiUserService, ApiRestaurantService, ApiReservationService, ApiDishService, ApiTableService, ApiCategoryService, ApiMenuService])(ApiBaseService);
export default new ApiService();
